<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Quote
Route::group(['namespace' => 'Quote', 'as' => 'quote.', 'prefix' => '{slug}/quote'], function(){
    // Pages
    Route::group(['namespace' => 'Pages'], function() {
        // Start
        Route::group(['middleware' => ['findQuote:true']], function() {
            Route::get('start/{quoteUid?}', 'StartController@view')->name('start');
            Route::post('start/save/{quoteUid?}', 'StartController@save')->name('save');
        });

        Route::group(['middleware' => ['findQuote', 'checkStage']], function() {
            // Room
            Route::get('room/{quoteUid}', 'RoomController@view')->name('room');
            Route::post('room/{quoteUid}/save', 'RoomController@save')->name('room.save');

            // Menu
            Route::get('menu/{quoteUid}', 'MenuController@view')->name('menu');
            Route::post('menu/{quoteUid}/save', 'MenuController@save')->name('menu.save');

            // Menu Additions
            Route::get('additions/{quoteUid}', 'MenuAdditionsController@view')->name('additions');
            Route::post('additions/{quoteUid}/save', 'MenuAdditionsController@save')->name('additions.save');

            // Finish
            Route::get('finish/{quoteUid}', 'FinishController@view')->name('finish');
            Route::post('finish/{quoteUid}/cont', 'FinishController@cont')->name('finish.continue');
            Route::post('email/{quoteUid}', 'FinishController@email')->name('finish.email');
        });
    });

    // Review
    Route::get('review/{quoteUid}', 'QuoteController@review')
        ->name('review')
        ->middleware('findQuote');
});




Route::get('/home', 'HomeController@show');


Route::group(['namespace' => 'Admin', 'as' => 'admin.', 'prefix' => 'admin'], function() {
    Route::group(['as' => 'rooms.', 'prefix' => 'rooms'], function() {
        Route::get('', 'RoomController@index')->name('index');
        Route::get('add', 'RoomController@add')->name('add');
        Route::post('', 'RoomController@store')->name('store');
        Route::get('edit/{id?}', 'RoomController@edit')->name('edit');
        Route::get('{id}', 'RoomController@show')->name('show');
        Route::put('{id}', 'RoomController@update')->name('update');
        Route::delete('{id}', 'RoomController@destroy')->name('destroy');
    });

    Route::group(['as' => 'menus.', 'prefix' => 'menus'], function() {
        Route::get('', 'MenuController@index')->name('index');
        Route::get('add', 'MenuController@add')->name('add');
        Route::post('', 'MenuController@store')->name('store');
        Route::get('edit/{id?}', 'MenuController@edit')->name('edit');
        Route::get('{id}', 'MenuController@show')->name('show');
        Route::put('{id}', 'MenuController@update')->name('update');
        Route::delete('{id}', 'MenuController@destroy')->name('destroy');
    });

    Route::group(['as' => 'menuAdditions.', 'prefix' => 'menuAdditions'], function() {
        Route::get('', 'MenuAdditionsController@index')->name('index');
        Route::get('add', 'MenuAdditionsController@add')->name('add');
        Route::post('', 'MenuAdditionsController@store')->name('store');
        Route::get('edit/{id?}', 'MenuAdditionsController@edit')->name('edit');
        Route::get('{id}', 'MenuAdditionsController@show')->name('show');
        Route::put('{id}', 'MenuAdditionsController@update')->name('update');
        Route::delete('{id}', 'MenuAdditionsController@destroy')->name('destroy');
    });

    Route::group(['as' => 'quotes.', 'prefix' => 'quotes'], function() {
        Route::get('', 'QuoteController@index')->name('index');
        Route::get('add', 'QuoteController@add')->name('add');
//        Route::post('', 'QuoteController@store')->name('store');
//        Route::get('edit/{id?}', 'QuoteController@edit')->name('edit');
        Route::get('{id}', 'QuoteController@show')->name('show');
//        Route::put('{id}', 'QuoteController@update')->name('update');
//        Route::delete('{id}', 'QuoteController@destroy')->name('destroy');
    });
});
