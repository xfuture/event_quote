if (window.jQuery) {
    $( document ).ready(function() {
        //Phone format
        $('#phone').mask('XXXX XXX XXX', {
            translation: {
                'X': {
                    pattern: /[0-9]/, optional: true
                }
            }
        });
        //Date time picker
        $('#datetimepicker').datepicker({
            minDate: 0,
            dateFormat: 'dd/mm/yy'
        });

        //Room quote page
        $('.location-item').on('click', function(){
            $('.location-item').css({"border-width":"0"});
            $(this).css({"border-width":"1px"});

            $(this).find('span.image-radio input').attr('checked',true);
        });

        // $('a.switch-step').on('click', function (e) {
        //     $("#dialog").dialog({
        //         buttons : {
        //             "Confirm" : function() {
        //                 //TODO Post form to save changes by ajax
        //             },
        //             "Cancel" : function() {
        //                 $(this).dialog("close");
        //             }
        //         }
        //     });
        //
        //     $("#dialog").dialog("open");
        // })

        //Confirm modal
        $("#dialog").dialog({
            autoOpen: false,
            modal: true
        });
    });
}else{
    console.log('Cannot load jquery');
}