@extends('spark::layouts.app')

@section('content')
<div class="container" style="width: 100%">
<div class="panel panel-default">
    <h3 class="panel-heading">{{ isset($menu) ? "Edit Menu" : "Add New Menu" }}</h3>

    <div class="panel-body" style="padding: 0 1500px 1100px 100px">
        @if(isset($menu))
            {!! Form::model($menu, ['route' => ['admin.menus.update', $menu->id], 'method' => 'PUT']) !!}
        @else
            {!! Form::open(['route' => 'admin.menus.store']) !!}
        @endif
            <div class="form-group">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name', null,  ['class' => 'form-control']) !!}
                <span class="is-danger">{{ $errors->first('name') }}</span>
            </div>

            <div class="form-group">
                {!! Form::label('price_pp', 'Price (per person)') !!}
                {!! Form::number('price_pp', null, ['min'=>0]) !!}
                <span class="is-danger">{{ $errors->first('price_pp') }}</span>
            </div>

            @foreach(['features', 'abstract', 'description'] as $key)
                <div class="form-group">
                    {!! Form::label($key, title_case($key)) !!}
                    {!! Form::textarea($key, null,  ['class' => 'form-control']) !!}
                    <span class="is-danger">{{ $errors->first($key) }}</span>
                </div>
            @endforeach

            @foreach(['min_persons', 'max_persons'] as $key)
                <div class="form-group">
                    {!! Form::label($key, title_case($key)) !!}
                    {!! Form::number($key, null, ['min'=>1]) !!}
                    <span class="is-danger">{{ $errors->first($key) }}</span>
                </div>
            @endforeach

            <div class="form-group">
                {!! Form::hidden('active', 0) !!}
                {!! Form::checkbox('active', 1, isset($menu) ? $menu->active : true) !!}
                {!! Form::label('active', 'Active') !!}
                <span class="is-danger">{{ $errors->first('active') }}</span>
            </div>

            <div class="form-group">
                {!! Form::label('order', 'Order') !!}
                {!! Form::number('order', null, ['class' => 'form-control']) !!}
                <p class="help-block">Choose a number to select which position this new room will be displayed in or leave as zero to place it last.</p>
                <span class="is-danger">{{ $errors->first('order') }}</span>
            </div>

            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}

            @if(isset($menu))
                <a href="{{ route('admin.menus.show', $menu->id) }}" class="btn">
                    Cancel
                </a>
            @else
                <a href="{{ route('admin.menus.index') }}" class="btn">
                    Cancel
                </a>
            @endif
        {!! Form::close() !!}
    </div>
</div>
</div>
@endsection

