@extends('spark::layouts.app')

@section('content')
    <div class="container" style="width: 100%">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ route('admin.menus.add') }}" class="btn btn-primary">Add New Menu</a>
            </div>
            <div class="panel-body">
                <table border="1">
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Features</th>
                        <th>Description</th>
                        <th>Abstract</th>
                        <th>Size</th>
                        <th></th>
                    </tr>
                    @foreach($menus as $menu)
                        <tr>
                            <td>
                                <p>{{ $menu->name ? $menu->name : 'N/A' }}</p>
                            </td>

                            <td>
                                <p>${{ $menu->price_pp ? $menu->price_pp : 'N/A' }}</p>
                            </td>

                            <td>
                                <p>{{ $menu->features ? $menu->features : 'N/A' }}</p>
                            </td>

                            <td>
                                <p>
                                    {!! nl2br(e($menu->description ? $menu->description : 'N/A')) !!}
                                </p>
                            </td>

                            <td>
                                <p>{{ $menu->abstract ? $menu->abstract : 'N/A' }}</p>
                            </td>

                            <td>
                                <p>{{ $menu->min_persons ? $menu->min_persons : 'N/A' }}-{{ $menu->max_persons ? $menu->max_persons : 'N/A' }}</p>
                            </td>

                            <td style="width: 180px">
                                <a href="{{ route('admin.menus.edit', $menu->id) }}" class="btn btn-primary">Edit</a>

                                {!! Form::open(['method' => 'delete', 'route' => ['admin.menus.destroy', $menu->id], 'style'=>'float:right; height:0']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection

