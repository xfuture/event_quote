@extends('spark::layouts.app')

@section('content')
    <div class="container" style="width: 100%">
        <div class="panel panel-default">
            <h3 class="panel-heading">{{ isset($room) ? "Edit Room" : "Add New Room" }}</h3>

            <div class="panel-body" style="padding: 0 1500px 900px 100px">
                @if(isset($room))
                    {!! Form::model($room, ['route' => ['admin.rooms.update', $room->id], 'method' => 'PUT']) !!}
                @else
                    {!! Form::open(['route' => 'admin.rooms.store']) !!}
                @endif
                <div>
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name', null,  ['class' => 'form-control']) !!}
                    <span class="is-danger">{{ $errors->first('name') }}</span>
                </div>

                @foreach(['caption', 'abstract', 'description'] as $key)
                    <div>
                        {!! Form::label($key, title_case($key)) !!}
                        {!! Form::textarea($key, null,  ['class' => 'form-control']) !!}
                        <span class="is-danger">{{ $errors->first($key) }}</span>
                    </div>
                @endforeach

                <div>
                    {!! Form::hidden('active', 0) !!}
                    {!! Form::checkbox('active', 1, isset($room) ? $room->active : true) !!}
                    {!! Form::label('active', 'Active') !!}
                    <span class="is-danger">{{ $errors->first('active') }}</span>
                </div>

                <div>
                    {!! Form::label('order', 'Order') !!}
                    {!! Form::number('order', null, ['class' => 'form-control', 'min'=>0]) !!}
                    <p class="help-block">Choose a number to select which position this new room will be displayed
                        in or leave as zero to place it last.</p>
                    <span class="is-danger">{{ $errors->first('order') }}</span>
                </div>

                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}

                @if(isset($room))
                    <a href="{{ route('admin.rooms.show', $room->id) }}" class="btn">
                        Cancel
                    </a>
                @else
                    <a href="{{ route('admin.rooms.index') }}" class="btn">
                        Cancel
                    </a>
                @endif
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

