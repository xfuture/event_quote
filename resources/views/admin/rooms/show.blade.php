@extends('spark::layouts.app')

@section('content')
    <div class="container">
            <div class="panel panel-default">
                <h3 class="panel-heading">
                    {{ $room->name }}
                </h3>

                <div class="panel-body" style="">
                    <strong>Abstract</strong>
                    <p>{{ $room->abstract }}</p>

                    <strong>Caption</strong>
                    <p>{{ $room->caption }}</p>

                    <strong>Description</strong>
                    <p>{{ $room->description }}</p>

                    <strong>Owner</strong>
                    <p>{{ $room->owner->name }}</p>

                    {{--<a href="{{ route('admin.rooms.edit', $room->id) }}" class="btn btn-primary">Edit</a>--}}

                    {{--{!! Form::open(['method' => 'delete', 'route' => ['admin.rooms.destroy', $room->id], 'style'=>'float:right']) !!}--}}
                        {{--{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}--}}
                    {{--{!! Form::close() !!}--}}
                </div>
        </div>
    </div>
@endsection

