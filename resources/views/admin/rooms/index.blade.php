@extends('spark::layouts.app')

@section('content')
    <div class="container" style="width: 100%">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ route('admin.rooms.add') }}" class="btn btn-primary">Add New Room</a>
            </div>

            <div class="panel-body" style="">
                <table border="1">
                    <tr>
                        <th>Name</th>
                        <th>Owner</th>
                        <th>Caption</th>
                        <th>Description</th>
                        <th>Picture</th>
                        <th></th>
                    </tr>
                    @foreach($rooms as $room)
                        <tr>
                            <td>
                                <p>{{ $room->name ? $room->name : 'N/A'  }}</p>
                            </td>

                            <td>
                                @if(!empty($room->owner))
                                    <p>{{ $room->owner->name ? $room->owner->name : "N/A" }}</p>
                                @else
                                    N/A
                                @endif
                            </td>

                            <td>
                                <p>{{ $room->caption ? $room->caption : 'N/A'  }}</p>
                            </td>

                            <td>
                                <p>{{ $room->description ? $room->description : 'N/A' }}</p>
                            </td>

                            <td>
                                <img src="{{ $room->abstract  }}" height="100" width="100">
                            </td>

                            <td style="width: 180px">
                                <a href="{{ route('admin.rooms.edit', $room->id) }}" class="btn btn-primary">Edit</a>

                                {!! Form::open(['method' => 'delete', 'route' => ['admin.rooms.destroy', $room->id], 'style'=>'float:right; height:0']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection

