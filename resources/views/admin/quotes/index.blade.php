@extends('spark::layouts.app')

@section('content')
    <div class="container" style="width: 100%">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ route('admin.quotes.add') }}" class="btn btn-primary">Add New Quote</a>
            </div>
            <div class="panel-body" style="">
                <table border="1">
                    <tr>
                        <th>UID</th>
                        <th>Customer</th>
                        <th>Room</th>
                        <th>Menu</th>
                        <th>Menu Additions</th>
                        <th>Number of people</th>
                        <th>Date</th>
                    </tr>
                    @foreach($quotes as $quote)
                        <tr>
                            <td width="10px">
                                <p>{{ $quote->quote_uid }}</p>
                            </td>

                            <td>
                                @if(!empty($quote->user))
                                    <p>{{ $quote->user->name ? $quote->user->name : "N/A" }}</p>
                                @else
                                    N/A
                                @endif
                            </td>

                            <td>
                                @if(!empty($quote->room))
                                    <a href="{{ route('admin.rooms.edit', $quote->room->id) }}">{{ $quote->room->name }}</a>
                                @else
                                    N/A
                                @endif
                            </td>

                            <td>
                                @if(!empty($quote->menu))
                                    <a href="{{ route('admin.menus.edit', $quote->menu->id) }}">{{ $quote->menu->name }}</a>
                                @else
                                    N/A
                                @endif
                            </td>

                            <td>
                                @if($quote->menuAdditions->count())
                                    <ul>
                                        @foreach($quote->menuAdditions as $menuAddition)
                                            <li>
                                                <a href="{{ route('admin.menuAdditions.edit', $menuAddition->id) }}">{{ $menuAddition->name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @else
                                    N/A
                                @endif
                            </td>

                            <td>
                                {{ $quote->number_people }}
                            </td>

                            <td>
                                {{ $quote->getFormattedDate() }}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection

