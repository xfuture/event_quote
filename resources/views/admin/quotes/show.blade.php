@extends('spark::layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p><strong>Customer:</strong> {{ $quote->owner->name }}</p>
                    <p><strong>Date:</strong> {{ $quote->date }}</p>
                    <p><strong>Number of people:</strong> {{ $quote->number_people }}</p>
                    <p><strong>Room:</strong> {{ $quote->room->name }}</p>
                    <p><strong>Menu:</strong> {{ $quote->menu->name }}</p>

                    {{--<a href="{{ route('admin.quotes.edit', $quote->id) }}" class="btn btn-primary">Edit</a>--}}

                    {{--{!! Form::open(['method' => 'delete', 'route' => ['admin.quotes.destroy', $quote->id]]) !!}--}}
                        {{--{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}--}}
                    {{--{!! Form::close() !!}--}}
                </div>
            </div>
        </div>
    </div>
@endsection

