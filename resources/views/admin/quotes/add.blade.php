@extends('spark::layouts.app')

@section('content')
    @php
    $teams = [Auth::user()->currentTeam()];
    @endphp
<div class="container" style="width: 100%">
    <div class="panel panel-default">
        <div class="panel-heading">
            Add New Quote
        </div>
        <div class="panel-body">
            @if(count($teams))
                <table border="1">
                    <tr>
                        <th>Id</th>
                        <th>Owner</th>
                        <th>Name</th>
                        <th>Slug</th>
                        <th></th>
                    </tr>
                    @foreach($teams as $team)
                        <tr>
                            <td width="10px">
                                {{ $team->id }}
                            </td>

                            <td>
                                @if(!empty($team->owner))
                                    {{ $team->owner->name ? $team->owner->name : "N/A" }}
                                @else
                                    N/A
                                @endif
                            </td>

                            <td>
                                {{ $team->name ? $team->name : "N/A" }}
                            </td>

                            <td>
                                {{ $team->slug ? $team->slug : "N/A" }}
                            </td>

                            <td>
                                @if(count($team->menus))
                                <a href="{{ route('quote.start', $team->slug) }}" target="_blank" class="btn btn-primary">Add</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            @else
                No teams ...
            @endif
        </div>
    </div>

</div>
@endsection

