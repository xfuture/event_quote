@extends('spark::layouts.app')

@section('content')
    <div class="container" style="width: 100%">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ route('admin.menuAdditions.add') }}" class="btn btn-primary">Add New Menu Addition</a>
            </div>
            <div class="panel-body" style="">
                <table border="1">
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                    @foreach($menuAdditions as $menuAddition)
                        <tr>
                            <td>
                                <p>
                                    {{ $menuAddition->name }}
                                </p>
                            </td>

                            <td>
                                <p>${{ $menuAddition->price_pp ? $menuAddition->price_pp : 'N/A' }}</p>
                            </td>

                            <td>
                                <p>{{ $menuAddition->description ? $menuAddition->description : 'N/A' }}</p>
                            </td>

                            <td style="width: 180px">
                                <a href="{{ route('admin.menuAdditions.edit', $menuAddition->id) }}" class="btn btn-primary">Edit</a>

                                {!! Form::open(['method' => 'delete', 'route' => ['admin.menuAdditions.destroy', $menuAddition->id], 'style'=>'float:right; height:0']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>

        </div>


    </div>
@endsection

