@extends('spark::layouts.app')

@section('content')
<div class="container" style="width: 100%">
<div class="panel panel-default">
    <h3 class="panel-heading">{{ isset($menuAddition) ? "Edit Menu Addition" : "Add New Menu Addition" }}</h3>

    <div class="panel-body" style="padding: 0 1500px 450px 100px">
        @if(isset($menuAddition))
            {!! Form::model($menuAddition, ['route' => ['admin.menuAdditions.update', $menuAddition->id], 'method' => 'PUT']) !!}
        @else
            {!! Form::open(['route' => 'admin.menuAdditions.store']) !!}
        @endif
            <div class="form-group">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name', null,  ['class' => 'form-control']) !!}
                <span class="is-danger">{{ $errors->first('name') }}</span>
            </div>

            <div class="form-group">
                {!! Form::label('price_pp', 'Price (per person)') !!}
                {!! Form::number('price_pp', null, ['min'=>0]) !!}
                <span class="is-danger">{{ $errors->first('price_pp') }}</span>
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Description') !!}
                {!! Form::textarea('description', null,  ['class' => 'form-control']) !!}
                <span class="is-danger">{{ $errors->first('description') }}</span>
            </div>

            {{--@foreach(['min_persons', 'max_persons'] as $key)--}}
                {{--<div class="form-group">--}}
                    {{--{!! Form::label($key, title_case($key)) !!}--}}
                    {{--{!! Form::number($key, null) !!}--}}
                {{--</div>--}}
            {{--@endforeach--}}

            <div class="form-group">
                {!! Form::hidden('active', 0) !!}
                {!! Form::checkbox('active', 1, isset($menuAddition) ? $menuAddition->active : true) !!}
                {!! Form::label('active', 'Active') !!}
                <span class="is-danger">{{ $errors->first('active') }}</span>
            </div>

            <div class="form-group">
                {!! Form::label('order', 'Order') !!}
                {!! Form::number('order', null, ['class' => 'form-control', 'min'=>0]) !!}
                <p class="help-block">Choose a number to select which position this new room will be displayed in or leave as zero to place it last.</p>
                <span class="is-danger">{{ $errors->first('order') }}</span>
            </div>

            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}

            @if(isset($menu))
                <a href="{{ route('admin.menuAdditions.show', $menuAddition->id) }}" class="btn">
                    Cancel
                </a>
            @else
                <a href="{{ route('admin.menuAdditions.index') }}" class="btn">
                    Cancel
                </a>
            @endif
        {!! Form::close() !!}
    </div>
</div>
</div>
@endsection

