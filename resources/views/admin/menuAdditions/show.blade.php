@extends('spark::layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <h3 class="panel-heading">
                    {{ $menuAddition->name }}
                </h3>

                <div class="panel-body">
                    @foreach(['price_pp', 'description'] as $key)
                        <strong>{{ title_case($key) }}</strong>
                        <p>{{ $menuAddition[$key] }}</p>
                    @endforeach
                    {{--<a href="{{ route('admin.menuAdditions.edit', $menuAddition->id) }}" class="btn btn-primary">Edit</a>--}}

                    {{--{!! Form::open(['method' => 'delete', 'route' => ['admin.menuAdditions.destroy', $menuAddition->id], 'style'=>'float:right']) !!}--}}
                        {{--{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}--}}
                    {{--{!! Form::close() !!}--}}
                </div>
            </div>
        </div>
    </div>
@endsection

