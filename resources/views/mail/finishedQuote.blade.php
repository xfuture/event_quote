@php
    $quoteTotal = 0.00;

    if(isset($quote) && !empty($quote)){
        $menu = isset($quote) ? $quote->menu : null;
        if(!empty($menu)){
            $quoteTotal += $menu->price_pp;
        }

        $menuAdditions = isset($quote) ? $quote->menuAdditions : null;
        foreach ($menuAdditions as $menuAddition){
            $quoteTotal += $menuAddition->price_pp * $quote->number_people;
        }
    }

@endphp
<html>
<head>
    <title>Finished quote</title>
</head>
<body>
    <h2>Here is a copy of the finished quote:</h2>
    <table border="1">
        <tr>
            <th>UID</th>
            <th>Customer</th>
            <th>Room</th>
            <th>Size</th>
            <th>Menu</th>
            <th>Menu Additions</th>
            <th>Total</th>
        </tr>
            <tr>
                <td width="10px" style="text-align: center">
                    {{ $quote->quote_uid }}
                </td>

                <td>
                    @if(!empty($quote->user))
                        {{ $quote->user->name }}
                    @else
                        N/A
                    @endif
                </td>

                <td>
                    @if(!empty($quote->room))
                        {{ $quote->room->name }}
                    @else
                        N/A
                    @endif
                </td>

                <td style="text-align: center">
                    @if(!empty($quote->number_people))
                        {{ $quote->number_people }} (people)
                    @else
                        N/A
                    @endif
                </td>

                <td>
                    @if(!empty($quote->menu))
                        {{ $quote->menu->name }}: ${{ $quote->menu->price_pp }}
                    @else
                        N/A
                    @endif
                </td>

                <td>
                    @if($quote->menuAdditions->count())
                        <ul>
                            @foreach($quote->menuAdditions as $menuAddition)
                                <li>
                                    {{ $menuAddition->name }}: ${{ $menuAddition->price_pp }} (pp)
                                </li>
                            @endforeach
                        </ul>
                    @else
                        N/A
                    @endif
                </td>

                <td style="text-align: center;">
                    <b>${{ \EventQuote\Services\DisplayService::displayCurrency($quoteTotal) }}</b>
                </td>
            </tr>
    </table>
</body>
</html>