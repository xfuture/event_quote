@extends('layouts.quote')

@section('content')
    @include($currentQuotePage::$template)
@endsection

@section('summary')
    @include($currentQuotePage::$summary)
@endsection