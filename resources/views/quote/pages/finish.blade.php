<h2>5. Finish Quick Quote</h2>
<p class="step-desc">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
</p>

{!! Form::open([
    'route' => ['quote.finish.email', $team->id, $quote->quote_uid],
    'method' => 'POST',
    'id'=>'email_form'
]) !!}
<div id="email-a-friend">
    {!! Form::label('email', 'Email a friend') !!}
    {!! Form::text('email', null, ['placeholder' => 'Enter Email']) !!}
    <div class="row">
        <div class="large-12 columns">
            <a href="" id="send-email-button" onclick="$('#email_form').submit();">Send</a>
        </div>
    </div>
</div>
{!! Form::close() !!}