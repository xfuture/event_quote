<h2>1. Contact and Event Information</h2>
<p class="step-desc">
	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
</p>

<!-- Form Section -->
<div id="form-section" class="row">
	<!-- Left Side -->
	<div class="large-6 columns left">
		<div class="row">
			<div class="large-12 columns">
				{!! Form::label('name', 'Contact Name') !!}
				{!! Form::text('name', $quote ? $quote->owner->name : null, ['placeholder' => 'Enter Full Name', 'v-model' => 'name']) !!}
				<span class="is-danger">{{ $errors->first('name') }}</span>
			</div>

			<div class="large-12 columns">
				{!! Form::label('email', 'Email') !!}
				{!! Form::text('email', $quote ? $quote->owner->email : null, ['placeholder' => 'Enter Email', 'v-model' => 'email', 'style'=>'background-color : white']) !!}
				<span class="is-danger">{{ $errors->first('email') }}</span>
			</div>

			<div class="large-12 columns">
				{!! Form::label('phone', 'Mobile') !!}
				{!! Form::text('phone', $quote ? $quote->owner->phone : null, ['placeholder' => '0400 000 000']) !!}
				<span class="is-danger">{{ $errors->first('phone') }}</span>
			</div>
		</div>
	</div>

	<!-- Right Side -->
	<div class="large-6 columns">
		<div class="large-12 columns">
			{!! Form::label('function_type_id', 'Type of Function') !!}
			{!! Form::functionTypeSelect(null, 'function_type_id', $quote ? $quote->functionType->id : null, ['placeholder' => 'Select Function Type', 'v-model' => 'function_type_id']) !!}
			<span class="is-danger">{{ $errors->first('function_type_id') }}</span>
		</div>

		<div class="large-12 columns">
			{!! Form::label('date', 'Event Date') !!}
			{!! Form::dateSelect('date', $quote ? $quote->getFormattedDate() : null, ['placeholder' => 'dd/mm/yyyy', 'v-model' => 'date', 'id'=>'datetimepicker', 'readonly'=>true, 'style'=>'background-color:white', 'onfocus'=>'this.blur()']) !!}
			<span class="is-danger">{{ $errors->first('date') }}</span>
		</div>

		<div class="large-12 columns">
			{!! Form::label('number_people', 'No. of People') !!}
			{!! Form::number('number_people', $quote ? $quote->number_people : null, ['placeholder' => 'Enter No. of People', 'min'=>1]) !!}
			<span class="is-danger">{{ $errors->first('number_people') }}</span>
		</div>
	</div>
</div>

