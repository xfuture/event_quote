<h2>4. Menu Additions</h2>
<p class="step-desc">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
</p>

<h3 class="sub-title">Select Your Menu Additions</h3>
<div id="additions-list" class="scroll">
    <ul id="menu-additions">
        @foreach ($menuAdditions as $addition)
            <div>
                <li class="checkbox-list-item">
                    <div class="row">
                        <div class="large-1 columns">
                            {!! Form::checkbox('menu_addition_ids[]', $addition->id, $quote->menuAdditions->contains($addition), ['class'=>'additions-checkbox']) !!}
                        </div>
                        <div class="large-11 columns">
                            <label for="item1">
                                <span class="addition-description">{{$addition->description}}</span>
                                <span class="addition-price">${{ $addition->price_pp }} pp</span>
                            </label>
                        </div>
                    </div>
                </li>
            </div>
        @endforeach
    </ul>
</div>