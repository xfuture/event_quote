<h2>3. Menu Packages</h2>
<p class="step-desc">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
</p>

<h3 class="sub-title">Select Your Menu Package</h3>
<div id="package-list" class="scroll">
    @php($hasMenu = false)

    @if(count($menus) > 0)
        @foreach ($menus as $menu)
            @if($quote->number_people >= $menu->min_persons && $quote->number_people <= $menu->max_persons)
                @php($hasMenu =true)
                <div class="row package-item" style="height: 50%">
                    <div class="large-1 columns">
                        {!! Form::radio('menu_id', $menu->id, $quote->menu && $quote->menu->id == $menu->id, ['id'=>'item-'.$menu->id, 'class'=>'package-select-button']) !!}
                    </div>
                    <label for="item-{{ $menu->id }}">
                        <div class="large-11 columns">
                            <span class="package-title">{{ $menu->name  }}: Starting from ${{ $menu->price_pp }}pp</span>
                            <ul class="package-contents-list" style="list-style: none;">
                                {{--<li class="package-details">{{ $menu->features  }}</li>--}}
                                {{--<li class="package-details">{{ $menu->abstract  }}</li>--}}
                                {{--<li class="package-details">{{ $menu->description  }}</li>--}}
                                <li class="package-details">{!! nl2br(e($menu->description)) !!}</li>
                            </ul>
                        </div>
                    </label>
                </div>
            @endif
        @endforeach
    @endif

    @if(!$hasMenu || count($menus) == 0)
        <div style="text-align: center">
            <p>Currently, there is no menu that suits your provided details.</p>
        </div>
    @endif

</div>