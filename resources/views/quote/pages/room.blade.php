<h2>2. Event Room / No. People</h2>
<p class="step-desc">
	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
</p>

<!-- Location Selection -->
<div class="page-main" style="height: 100%">
	<h3 class="sub-title">Select Your Event Room</h3>
	<div class="location-viewer">
		<div class="row">
			<div class="large-12 columns">
				<div class="location-list">
					<div class="row">
						@foreach($rooms as $room)
							<div class="large-6 columns location-item" style="border-color: #4f9af5; border-width : {{ ($quote->room && $quote->room->id == $room->id) ? 1 : 0 }}; border-style : solid">
								<span class="image-radio">
									{!! Form::radio('room_id', $room->id, $quote->room && $quote->room->id == $room->id, ['style'=>'display:none']) !!}
									<img src="{{ $room->abstract }}" alt="room-left" id="room-left" class="location-image">
								</span>
								<span class="location-title">{{ $room->name }}</span>
								<span class="location-brief-desc">{{ $room->caption }}</span>
								<p class="location-full-desc">
									{!! nl2br(e($room->description)) !!}
								</p>
								<br/>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>