@extends('spark::layouts.app')

@section('content')
<home :user="user" inline-template>
    <div class="container" style="width: 100%; text-align: center">
        <h1>Event Quote Dashboard</h1>
    </div>
</home>
@endsection
