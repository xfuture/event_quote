<html>
	<head>
		<!-- BugHerd script -->
		<script type='text/javascript'>
            (function (d, t) {
                var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
                bh.type = 'text/javascript';
                bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=dj7lfmifhjtpmcesyhxk6g';
                s.parentNode.insertBefore(bh, s);
            })(document, 'script');
		</script>

		<script src="{{ elixir('js/vendor.js') }}"></script>
		<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.8/vue.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
		{{--<script src="{{ elixir('js/app.js') }}"></script>--}}
		<script src="{{ elixir('js/app-main.js') }}"></script>
		<script src="{{ elixir('js/sweetalert.min.js') }}"></script>


{{--		<link rel="stylesheet" href="{{ elixir('css/spark.css') }}">--}}
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link rel="stylesheet" href="{{ elixir('css/sweetalert.css') }}">
		<link rel="stylesheet" href="{{ elixir('css/app.css') }}">
	</head>
	<body>
		<header>
			<div class="container">
				<div class="row">
					<div id="site-title" class="large-12 columns" style="text-align: center">
						<h1>Quick Calculator</h1>
					</div>
				</div>
			</div>
		</header>

		<section id="main__content">
			@if(count($team->menus))
				<div id="dialog" title="Confirmation Required">
					Are you sure to save changes?
				</div>

				<div class="container">
					<div class="row" id="main-row">
						<!-- Left -->
						<div class="large-8 columns">
							<div class="company__logo">
								<img id="logo" src="/img/zS-Dwf8M.jpg" alt="logo" width="50" height="50">
							</div>

							{{-- If the page has no data, put the contents outside of the form --}}
							@if($pageHasData === false)
								@yield('content')
							@endif

							{!! Form::open([
                                'route' => [
                                    $currentQuotePage::$saveRoute,
                                    $team ? $team->slug : null,
                                    $quote ? $quote->quote_uid : null
                                ],
                                'method' => 'POST',
                                'id'=>'quote-form'
                            ]) !!}

							@if($pageHasData !== false)
								@yield('content')
							@endif

							@include('_partials.quote.layout.footer')

							{!! Form::close() !!}
						</div>
						@yield('summary')
					</div>
				</div>
			@else
				<div style="text-align: center">At the moment, there is no available quote for this team</div>
			@endif
		</section>
	</body>
</html>