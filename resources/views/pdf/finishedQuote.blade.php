<html>
<head>
    <title>Finished quote</title>
</head>
<body>
<h2>Here is a copy of the finished quote:</h2>
<p>
    <strong>Date:</strong> {{ $quote->date }}
</p>
<p>
    <strong>Number of people:</strong> {{ $quote->number_people }}
</p>
<p>
    <strong>Room name:</strong> {{ $quote->room->name }}
</p>
<p>
    <strong>Room price (pp):</strong> {{ $quote->room->price_pp }}
</p>
<p>
    <strong>Menu name:</strong> {{ $quote->menu->name }}
</p>
<p>
    <strong>Menu cost:</strong> {{ $quote->menu->price_pp }}
</p>
</body>
</html>