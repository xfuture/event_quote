<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container" style="width: 100%">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <div class="hamburger">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#spark-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Branding Image -->
            @include('spark::nav.brand')
        </div>

        <div class="collapse navbar-collapse" id="spark-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li><a class="navbar-brand" href="/home"><h3 style="color: black">Event Quote | </h3></a></li>
                <li><a href="/login" class="navbar-link" style="color: #1779ba">Login</a></li>
                <li><a href="/register" class="navbar-link" style="color: #1779ba">Register</a></li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
            </ul>
        </div>
    </div>
</nav>
