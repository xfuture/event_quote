<!-- Left Side Of Navbar -->
<li><a class="navbar-brand" href="/home" style="color: #1779ba"><h3 style="color: black">Event Quote | </h3></a></li>
<li><a href="{{ route('admin.rooms.index') }}" style="color: #1779ba">Rooms</a></li>
<li><a href="{{ route('admin.menus.index') }}" style="color: #1779ba">Menus</a></li>
<li><a href="{{ route('admin.menuAdditions.index') }}" style="color: #1779ba">Menu Additions</a></li>
<li><a href="{{ route('admin.quotes.index') }}" style="color: #1779ba">Quotes</a></li>
