<!-- Footer -->
<div class="footer">
    <div class="left">
        <p>
            @if($hasPrevQuotePage)
                <button type="submit" name="redirect" value="prev" class="button-back">Back</button>
            @endif
        </p>
    </div>
    <nav id="step-tracker" class="middle" style="padding: 15px 0 0 0;">
        <ul>
            @foreach($quotePages as $page)
                @php
                    $pageStatus = 'inactive';
                    if($page['isCurrent']){
                        $pageStatus = 'active';
                    }elseif($page['canAccess']){
                        $pageStatus = 'completed';
                    }
                @endphp
                @if(($page['displayName'] != "Event Location" || count($team->rooms)) && ($page['displayName'] != "Additions" || count($team->menuAdditions)))
                    @if($pageStatus == 'completed')
                        <a href="{{ route($page['route'], [isset($team) ? $team->slug : null, isset($quote) ? $quote->quote_uid : null])  }}" class="switch-step">
                            <li id="step{{ $loop->index }}-list-item" class="step-tracker-list-item-{{ $pageStatus }}"></li>
                        </a>
                    @else
                        <li id="step{{ $loop->index }}-list-item" class="step-tracker-list-item-{{ $pageStatus }}"></li>
                    @endif
                @endif
            @endforeach
        </ul>
    </nav>

    @if($hasNextQuotePage)
        <div class="right">
            <p>
                <button type="submit" name="redirect" value="next" class="button-next">Next</button>
            </p>
        </div>
    @else
        <div class="right">
            <p>
                <button type="submit" name="redirect" value="finish" class="button-next">Finish</button>
            </p>
        </div>
    @endif
</div>
