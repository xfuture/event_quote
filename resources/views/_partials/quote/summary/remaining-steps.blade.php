@php
    $isRemaining = false;

    foreach($quotePages as $page){
        if(!$page['canAccess']){
            $isRemaining = true;
            break;
        }
    }
@endphp

@if($isRemaining)
<div id="remaining-steps">
    <ul>
    @foreach($quotePages as $page)
        @if(!$page['canAccess'] && ($page['displayName'] != "Event Location" || count($team->rooms)) && ($page['displayName'] != "Additions" || count($team->menuAdditions)))
            <li>
                <span id="step2-icon" class="step-icons">{{ $loop->index }}</span>
                <span id="step2-name" class="step-names">{{ $page['displayName'] }}</span>
            </li>
        @endif
    @endforeach
    </ul>
</div>
@endif