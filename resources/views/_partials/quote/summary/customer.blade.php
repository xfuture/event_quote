@php
$currentPage = $quotePages['Customer & Event Details'];
$user = isset($quote) ? $quote->user : null;
$functionType = isset($quote) ? $quote->functionType : null;
@endphp

@if($currentPage['canAccess'])
<div id="event-details-completed" class="current-step">
	<span class="completed-step-icon">{{ !$currentPage['isCurrent'] ? '&#10003;' : '1' }}</span>
	<h3 id="current-step-title"><a style="color: black" href="{{ route($currentPage['route'], [isset($team) ? $team->slug : null, isset($quote) ? $quote->quote_uid : null])  }}" class="switch-step">{{ $currentPage['displayName'] }}</a></h3>

	@if($currentPage['isCurrent'] && isset($quote))
	<ul>
		<li>
			<label for="name" class="info-labels">Name:</label>
			<span id="name" class="info-text">{{ empty($user) ? null : $user->name }}</span>
		</li>
		<li>
			<label for="email" class="info-labels">Email:</label>
			<span id="email" class="info-text">{{ empty($user) ? null : $user->email }}</span>
		</li>
		<li>
			<label for="mobile" class="info-labels">Mobile:</label>
			<span id="mobile" class="info-text">{{ empty($user) ? null : $user->phone }}</span>
		</li>
		<li>
			<label for="event" class="info-labels">Event:</label>
			<span id="event" class="info-text">{{ empty($functionType) ? null : $functionType->name }}</span>
		</li>
		<li>
			<label for="date" class="info-labels">Date:</label>
			<span id="date" class="info-text">{{ \EventQuote\Services\DisplayService::displayDate($quote->date) }}</span>
		</li>
		<li>
			<label for="guest-total" class="info-labels">No.</label>
			<span id="guest-total" class="info-text">{{ $quote->number_people }}</span>
		</li>
	</ul>
	@endif
</div>
@endif