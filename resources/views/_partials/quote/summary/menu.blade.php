@php
    $currentPage = $quotePages['Menu'];
    $menu = isset($quote) ? $quote->menu : null;
@endphp

@if($currentPage['canAccess'])
    <div id="menu-selection" class="current-step">
        <span class="completed-step-icon">{{ !$currentPage['isCurrent'] ? '&#10003;' : '3' }}</span>
        <h3 id="current-step-title"><a style="color: black" href="{{ route($currentPage['route'], [isset($team) ? $team->slug : null, isset($quote) ? $quote->quote_uid : null])  }}" class="switch-step">{{ $currentPage['displayName'] }}</a></h3>

        @if($currentPage['isCurrent'])
        <ul>
            <li>
                <label for="selected-menu" class="info-labels">Menu:</label>
                <span id="selected-menu" class="info-text">{{ empty($menu) ? null : $menu->abstract }}</span>
            </li>
        </ul>
        @endif
    </div>
@endif