@php
    $currentPage = $quotePages['Finish'];
@endphp

@if($currentPage['canAccess'])
    <div id="finish" class="current-step">
        <span class="completed-step-icon">{{ !$currentPage['isCurrent'] ? '&#10003;' : '5' }}</span>
        <h3 id="current-step-title"><a style="color: black" href="{{ route($currentPage['route'], [isset($team) ? $team->slug : null, isset($quote) ? $quote->quote_uid : null])  }}" class="switch-step">{{ $currentPage['displayName'] }}</a></h3>
    </div>
@endif