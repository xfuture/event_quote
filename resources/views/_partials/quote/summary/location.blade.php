@php
    $currentPage = $quotePages['Event Location'];
    $room = isset($quote) ? $quote->room : null;
@endphp

@if($currentPage['canAccess'])
    <div id="event-location" class="current-step">
        <span class="completed-step-icon">{{ !$currentPage['isCurrent'] ? '&#10003;' : '2' }}</span>
        <h3 id="current-step-title"><a style="color: black" href="{{ route($currentPage['route'], [isset($team) ? $team->slug : null, isset($quote) ? $quote->quote_uid : null])  }}" class="switch-step">{{ $currentPage['displayName'] }}</a></h3>

        @if($currentPage['isCurrent'] && isset($quote))
        <ul>
            <li>
                <label for="location-name" class="info-labels">Room:</label>
                <span id="location-name" class="info-text">{{ empty($room) ? null : $room->name }}</span>
            </li>
        </ul>
        @endif
    </div>
@endif
