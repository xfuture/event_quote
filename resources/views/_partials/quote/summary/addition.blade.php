@php
    $currentPage = $quotePages['Additions'];
    $menuAdditions = isset($quote) ? $quote->menuAdditions : null;
@endphp

@if($currentPage['canAccess'])
    <div id="additions" class="current-step">
        <span class="completed-step-icon">{{ !$currentPage['isCurrent'] ? '&#10003;' : '4' }}</span>
        <h3 id="current-step-title"><a style="color: black" href="{{ route($currentPage['route'], [isset($team) ? $team->slug : null, isset($quote) ? $quote->quote_uid : null])  }}" class="switch-step">{{ $currentPage['displayName'] }}</a></h3>

        @if($currentPage['isCurrent'])
        <ul>
            @foreach($menuAdditions as $menuAddition)
            <li>
                <span class="info-labels">add:</span>
                <span id="step4-name" class="info-text">{{ $menuAddition->name }}</span>
            </li>
            @endforeach
        </ul>
        @endif
    </div>
@endif