@php
    $quoteTotal = 0.00;

    if(isset($quote) && !empty($quote)){
        $menu = isset($quote) ? $quote->menu : null;
        if(!empty($menu)){
            $quoteTotal += $menu->price_pp;
        }

        $menuAdditions = isset($quote) ? $quote->menuAdditions : null;
        foreach ($menuAdditions as $menuAddition){
            $quoteTotal += $menuAddition->price_pp * $quote->number_people;
        }
    }

@endphp

<div id="quote-total">
    <div class="row">
        <div class="large-6 columns">
            <p id="total-text">Quote Total:</p>
        </div>
        <div class="large-6 columns">
            <p id="total-sum">$<span class="final-sum">{{ \EventQuote\Services\DisplayService::displayCurrency($quoteTotal) }}</span></p>
        </div>
        <p id="footer-text">* All quoted prices are inclusive of GST</p>
    </div>
</div>