<!-- Right -->
<div id="quote-summary" class="large-4 columns">
	<h2 id="title">Quote Summary</h2>

	@include('_partials.quote.summary.customer')

	@if(count($team->rooms))
		@include('_partials.quote.summary.location')
	@endif

	@include('_partials.quote.summary.menu')

	@if(count($team->menuAdditions))
		@include('_partials.quote.summary.addition')
	@endif

	@include('_partials.quote.summary.finish')

	@include('_partials.quote.summary.remaining-steps')

	@include('_partials.quote.summary.quote-total')
</div>