var elixir = require('laravel-elixir');
var path = require('path');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var paths = {
    sass: [
        // Foundation 6
        'node_modules/foundation-sites/scss',

        // Vendor
        'node_modules/font-awesome/scss',

        'resources/assets/sass'
    ],
    javascript: [
        // Core vendor
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/what-input/dist/what-input.js',
        './node_modules/foundation-sites/dist/js/foundation.min.js',

        // Foundation 6
        './node_modules/foundation-sites/js/foundation.core.js',
        './node_modules/foundation-sites/js/foundation.util.*.js',

        // Foundation 6,
        './node_modules/foundation-sites/js/foundation.equalizer.js',
        './node_modules/foundation-sites/js/foundation.offcanvas.js',
        './node_modules/foundation-sites/js/foundation.reveal.js',
        './node_modules/foundation-sites/js/foundation.abide.js'
    ]
};


elixir(function(mix) {
    mix.sass('app.scss', 'public/css/app.css', null, { includePaths: paths.sass })
        .scripts(paths.javascript, 'public/js/vendor.js')
        .scripts([
            './resources/assets/js/components/timepicker-addon.js',
            './resources/assets/js/components/quote.js'
        ], 'public/js/app-main.js', './')
        .copy('./resources/assets/img', 'public/img')
        .copy('./resources/assets/fonts', 'public/fonts')


        .less('app.less', 'public/css/spark.css')
        .webpack('app.js', null, null, {
            resolve: {
                modules: [
                    path.resolve(__dirname, 'vendor/laravel/spark/resources/assets/js'),
                    'node_modules'
                ]
            }
        })
        .copy('node_modules/sweetalert/dist/sweetalert.min.js', 'public/js/sweetalert.min.js')
        .copy('node_modules/sweetalert/dist/sweetalert.css', 'public/css/sweetalert.css');
});
