<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuAdditionQuoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_addition_quote', function (Blueprint $table) {
            $table->unsignedInteger('quote_id');
            $table->foreign('quote_id')->references('id')->on('quotes');
            $table->unsignedInteger('menu_addition_id');
            $table->foreign('menu_addition_id')->references('id')->on('menu_additions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_addition_quote');
    }
}
