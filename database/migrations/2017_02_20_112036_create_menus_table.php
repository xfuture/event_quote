<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menus', function (Blueprint $table)
		{
			$table->increments('id');
			
			$table->unsignedInteger('team_id');
			$table->foreign('team_id')->references('id')->on('teams');
			
			$table->string('name');
			$table->float('price_pp', 10, 2);
			$table->text('features');
			$table->text('abstract');
			$table->text('description');
			
			$table->integer('min_persons');
			$table->integer('max_persons');
			$table->integer('order');
			$table->boolean('active');
			
			$table->unsignedInteger('user_id');
			$table->foreign('user_id')->references('id')->on('users');
			
			$table->softDeletes();
			
			$table->timestamps();
		});
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('menus');
	}
}
