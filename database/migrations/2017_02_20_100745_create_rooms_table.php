<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rooms', function (Blueprint $table)
		{
			$table->increments('id');
			
			$table->unsignedInteger('team_id');
			$table->foreign('team_id')->references('id')->on('teams');
			
			$table->string('name');
			$table->text('caption');
			$table->text('abstract');
			$table->text('description');
			
			$table->integer('order');
			$table->boolean('active');
			
			$table->unsignedInteger('user_id');
			$table->foreign('user_id')->references('id')->on('users');
			
			$table->softDeletes();
			
			$table->timestamps();
		});
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('rooms');
	}
}
