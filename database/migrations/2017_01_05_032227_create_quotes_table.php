<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
	        $table->increments('id');
	
	        $table->string('quote_uid');
	
	        $table->unsignedInteger('customer_id')->nullable();
	        $table->foreign('customer_id')->references('id')->on('users');
	
	        $table->unsignedInteger('function_type_id')->nullable();
	        $table->foreign('function_type_id')->references('id')->on('function_types');
	        
	        $table->integer('date');
	        $table->integer('number_people');
	        
	        $table->boolean('active');
	        $table->tinyInteger('submitted')->nullable();
	
	        $table->softDeletes();
	
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
