<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoomAndMenuToQuotes extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('quotes', function (Blueprint $table)
		{
			$table->unsignedInteger('room_id')->nullable()->after('number_people');
			$table->foreign('room_id')->references('id')->on('rooms');
			
			$table->unsignedInteger('menu_id')->nullable()->after('room_id');
			$table->foreign('menu_id')->references('id')->on('menus');
		});
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('quotes', function (Blueprint $table)
		{
			$table->dropForeign('quotes_room_id_foreign');
			$table->dropColumn('room_id');
			$table->dropForeign('quotes_menu_id_foreign');
			$table->dropColumn('menu_id');
		});
	}
}
