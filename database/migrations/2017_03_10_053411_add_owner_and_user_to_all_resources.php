<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOwnerAndUserToAllResources extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menus', function(Blueprint $table) {
            $table->unsignedInteger('owner_id');
        });

        // Update all existing rows so that they reference existing users
        \DB::statement('UPDATE menus SET owner_id = user_id');

        Schema::table('menus', function(Blueprint $table) {
            $table->foreign('owner_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menus', function(Blueprint $table) {
            $table->dropForeign('menus_owner_id_foreign');
            $table->dropColumn('owner_id');
        });
    }
}
