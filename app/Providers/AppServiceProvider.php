<?php

namespace EventQuote\Providers;

use EventQuote\Services\MenuAdditionsService;
use EventQuote\Services\MenuService;
use EventQuote\Services\QuotePagesService;
use EventQuote\Services\QuoteService;
use EventQuote\Services\RoomService;
use EventQuote\Services\TeamService;
use EventQuote\Services\UserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('room', RoomService::class);
        $this->app->singleton('menu', MenuService::class);
        $this->app->singleton('menuAddition', MenuAdditionsService::class);
        $this->app->singleton('quote', QuoteService::class);
        $this->app->singleton('user', UserService::class);
        $this->app->singleton('team', TeamService::class);

        $this->app->singleton(QuotePagesService::class);
    }
}
