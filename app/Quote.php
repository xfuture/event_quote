<?php

namespace EventQuote;

use EventQuote\Traits\BelongsToTeam;
use EventQuote\Traits\TracksOwnerAndUpdater;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quote extends Model
{
    use SoftDeletes, BelongsToTeam, TracksOwnerAndUpdater;

	protected $fillable = [
		'quote_uid',
		'customer_id',
		'function_type_id',
		'date',
		'number_people',
	    'active',
	];

    public function user()
    {
        return $this->belongsTo('EventQuote\User', 'customer_id', 'id');
    }

    public function team()
    {
        return $this->belongsTo('EventQuote\Team', 'team_id', 'id');
    }

    public function functionType()
    {
        return $this->belongsTo('EventQuote\FunctionType');
    }

    public function room()
    {
        return $this->belongsTo('EventQuote\Room');
    }

    public function menu()
    {
        return $this->belongsTo('EventQuote\Menu');
    }

    public function menuAdditions()
    {
        return $this->belongsToMany('EventQuote\MenuAddition');
    }


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->ownerIDField = 'customer_id';
        $this->updaterIDField = null;
    }

    public function getFormattedDate()
    {
        return  date('d/m/Y', $this->date);
    }

    public function hasCompletedPage($pageBit)
    {
        return ($this->stage & $pageBit) > 0;
    }

    public function markPageComplete($pageBit)
    {
        $this->stage |= $pageBit;
    }
}
