<?php

namespace EventQuote;

use \Form;

Form::macro('dateSelect', function ($name, $selected = null, $params = [])
{
	return Form::text($name, $selected, $params);
});

Form::macro('functionTypeSelect', function (Team $team = null, $name, $selected = null, $params = [])
{
	$data          = [];
	$functionTypes = FunctionType::where('active', 1)
	                             ->whereNull('deleted_at');
	if ($team != null)
	{
		$functionTypes = $functionTypes->where('team_id', $team->id);
	}
	$functionTypes = $functionTypes->orderBy('name', 'asc')
	                               ->get();
	
	foreach ($functionTypes as $functionType)
	{
		$data[$functionType->id] = $functionType->name;
	}
	
	return Form::select($name, $data, $selected, $params);
});