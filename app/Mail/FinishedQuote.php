<?php

namespace EventQuote\Mail;

use EventQuote\Quote;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Dompdf\Dompdf;

class FinishedQuote extends Mailable
{
    use Queueable, SerializesModels;

    public $quote;

    protected $pdfString;

    /**
     * Create a new message instance.
     *
     * @param Quote $quote
     * @param string $attachmentPath Path to the pdf to attach to the email.
     */
    public function __construct(Quote $quote)
    {
        $this->quote = $quote;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $ownerName = $this->quote->owner ? $this->quote->owner->name : "N/A";
        $attachmentName = 'Quote for ' . $ownerName . ' (' . date('r') . ').pdf';
        $pdfString = view('mail.finishedQuote')->with('quote', $this->quote)->render();
        $team = $this->quote->team;

        $dompdf = new Dompdf();
        $dompdf->loadHtml($pdfString);
        $dompdf->render();

        return $this
            ->subject('A quote was sent to you from' . $team ? $team->name : 'N/A')
            ->attachData($dompdf->output(), $attachmentName, [
                'mime' => 'application/pdf'
            ])
            ->view('mail.finishedQuote');
    }
}
