<?php

namespace EventQuote\Http\Controllers\Admin;

use EventQuote\Http\Controllers\Controller;
use EventQuote\Services\QuoteService;

class QuoteController extends Controller
{
    /**
     * @var QuoteService
     */
    protected $quoteService;

    public function __construct(QuoteService $quoteService)
    {
        $this->quoteService = $quoteService;
    }

    /**
     * Display a listing of the quotes.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $quotes = $this->quoteService->get();
        return view('admin.quotes.index')
            ->with('quotes', $quotes);
    }


    public function add()
    {
        return view('admin.quotes.add');
    }

    /**
     * Displays a specific resource.
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $quote = $this->quoteService->find($id);
        return view('admin.quotes.show')
            ->with('quote', $quote);
    }
}