<?php

namespace EventQuote\Http\Controllers\Admin;

use EventQuote\Http\Requests\Admin\MenuRequest;
use EventQuote\Services\MenuService;
use Illuminate\Http\Request;
use EventQuote\Http\Controllers\Controller;

class MenuController extends Controller
{
    /**
     * @var MenuService
     */
    protected $menuService;


    public function __construct(MenuService $menuService)
    {
        $this->menuService = $menuService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $menus = $this->menuService->get();
        return view('admin.menus.index')
            ->with('menus', $menus);
    }

    public function add()
    {
        $view = view('admin.menus.edit');
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  MenuRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuRequest $request)
    {
        $data = $request->all();
        $newMenu = $this->menuService->create($data);
        return redirect()
            ->route('admin.menus.show', $newMenu->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $menu = $this->menuService->find($id);
        return view('admin.menus.show')
            ->with('menu', $menu);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = null)
    {
        $view = view('admin.menus.edit');
        if ($id) {
            $menu = $this->menuService->find($id);
            $view->with('menu', $menu);
        }
        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MenuRequest $request, $id)
    {
        $this->menuService->update($id, $request->all());
        return redirect()->route('admin.menus.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->menuService->delete($id);
        return redirect()->route('admin.menus.index');
    }
}
