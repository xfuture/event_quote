<?php

namespace EventQuote\Http\Controllers\Admin;

use EventQuote\Http\Requests\Admin\RoomRequest;
use EventQuote\Services\RoomService;
use Illuminate\Http\Request;
use EventQuote\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class RoomController extends Controller
{
    /**
     * @var RoomService
     */
    protected $roomService;


    public function __construct(RoomService $roomService) {
        $this->roomService = $roomService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $rooms = $this->roomService->get();
        return view('admin.rooms.index')
            ->with('rooms', $rooms);
    }

    public function add()
    {
        $view = view('admin.rooms.edit');
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RoomRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoomRequest $request)
    {
        $data = $request->all();
        $newRoom = $this->roomService->create($data);

        return redirect()
            ->route('admin.rooms.show', $newRoom->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $room = $this->roomService->find($id);
        return view('admin.rooms.show')
            ->with('room', $room);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = null)
    {
        $view = view('admin.rooms.edit');
        if ($id) {
            $room = $this->roomService->find($id);
            $view->with('room', $room);
        }
        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RoomRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoomRequest $request, $id)
    {
        $this->roomService->update($id, $request->all());
        return redirect()->route('admin.rooms.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->roomService->delete($id);
        return redirect()->route('admin.rooms.index');
    }
}
