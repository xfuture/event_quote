<?php

namespace EventQuote\Http\Controllers\Admin;

use EventQuote\Http\Requests\Admin\MenuAdditionRequest;
use EventQuote\Services\MenuAdditionsService;
use Illuminate\Http\Request;
use EventQuote\Http\Controllers\Controller;
use EventQuote\Http\Requests\Admin\MenuRequest;

class MenuAdditionsController extends Controller
{
    /**
     * @var MenuAdditionsService
     */
    protected $menuAdditionsService;


    public function __construct(MenuAdditionsService $menuAdditionsService)
    {
        $this->menuAdditionsService = $menuAdditionsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $additions = $this->menuAdditionsService->get();
        return view('admin.menuAdditions.index')
            ->with('menuAdditions', $additions);
    }

    public function add()
    {
        $view = view('admin.menuAdditions.edit');
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  MenuRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuAdditionRequest $request)
    {
        $data = $request->all();

        $newAddition = $this->menuAdditionsService->create($data);
        return redirect()
            ->route('admin.menuAdditions.show', $newAddition->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $additions = $this->menuAdditionsService->find($id);
        return view('admin.menuAdditions.show')
            ->with('menuAddition', $additions);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = null)
    {
        $view = view('admin.menuAdditions.edit');
        if ($id) {
            $addition = $this->menuAdditionsService->find($id);
            $view->with('menuAddition', $addition);
        }
        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  MenuRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MenuRequest $request, $id)
    {
        $this->menuAdditionsService->update($id, $request->all());
        return redirect()->route('admin.menuAdditions.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->menuAdditionsService->delete($id);
        return redirect()->route('admin.menuAdditions.index');
    }
}
