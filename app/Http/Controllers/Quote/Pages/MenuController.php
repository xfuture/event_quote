<?php

namespace EventQuote\Http\Controllers\Quote\Pages;

use EventQuote\Http\Requests\Quote\MenuRequest;
use EventQuote\Facades\Menu;
use EventQuote\Services\MailService;
use Illuminate\Http\Request;
use EventQuote\Http\Controllers\Quote\QuotePageController;

class MenuController extends QuotePageController
{
    public static $template = 'quote.pages.menu';
    public static $viewRoute = 'quote.menu';
    public static $saveRoute = 'quote.menu.save';
    public static $displayName = 'Menu';
    public static $summary = '_partials.quote.summary';
    public static $id = 4;


    public function view(Request $request)
    {
        $menus = Menu::getPublic();
        return parent::view($request)
            ->with('menus', $menus);
    }


    public function save(MenuRequest $request)
    {
        $quote = $request->input('quote');
        $team = $request->input('team');
        $quote->menu_id = $request->input('menu_id');

        if(!count($team->menuAdditions)){
            MailService::mailFinishedQuote($quote);
        }

        return parent::saveAndContinue($request, $quote, $team);
    }
}
