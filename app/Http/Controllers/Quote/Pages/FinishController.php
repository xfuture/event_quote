<?php

namespace EventQuote\Http\Controllers\Quote\Pages;

use Barryvdh\Snappy\Facades\SnappyPdf;
use EventQuote\Http\Requests\Quote\EmailFriendRequest;
use EventQuote\Mail\FinishedQuote;
use EventQuote\Quote;
use Illuminate\Http\Request;
use EventQuote\Http\Controllers\Quote\QuotePageController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class FinishController extends QuotePageController
{
    public static $template = 'quote.pages.finish';
    public static $viewRoute = 'quote.finish';
    public static $saveRoute = 'quote.finish.continue';
    public static $displayName = 'Finish';
    public static $summary = '_partials.quote.summary';
    public static $id = 16;


    public function view(Request $request)
    {
        $quote = $request->input('quote');
        $quote->submitted = true;
        $quote->save();

        return parent::view($request)
            ->with('pageHasData', false);
    }


    public function cont(Request $request)
    {
//        return $this->pagesService->cont($this, $request, $request->quote);
    }


    public function email(EmailFriendRequest $request, $quoteUid)
    {
        $quote = $request->input('quote');

        // Create a pdf
        $pdfString = SnappyPdf::loadView('pdf.finishedQuote', ['quote' => $quote])->output();

        // Send email
        Mail::to(['email' => $request->email])
            ->send(new FinishedQuote($quote, $pdfString));

        return redirect()
            ->route(self::$viewRoute, $quoteUid)
            ->with('flashMessage', 'Email Sent!');
    }
}
