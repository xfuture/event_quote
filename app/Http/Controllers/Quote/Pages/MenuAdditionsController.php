<?php

namespace EventQuote\Http\Controllers\Quote\Pages;

use EventQuote\Facades\MenuAddition;
use EventQuote\Http\Requests\Quote\MenuAdditionRequest;
use EventQuote\Services\MailService;
use Illuminate\Http\Request;
use EventQuote\Http\Controllers\Controller;
use EventQuote\Http\Controllers\Quote\QuotePageController;

class MenuAdditionsController extends QuotePageController
{
    public static $template = 'quote.pages.additions';
    public static $viewRoute = 'quote.additions';
    public static $saveRoute = 'quote.additions.save';
    public static $displayName = 'Additions';
    public static $summary = '_partials.quote.summary';
    public static $id = 8;


    public function view(Request $request)
    {
        $team = $request->input("team");

        if(!empty($team) && count($team->menuAdditions)){
            $quote = $request->input('quote');
            $menuAdditions = MenuAddition::getAllActiveMenuAdditions($quote->menu);
            return parent::view($request)
                ->with('menuAdditions', $menuAdditions);
        }else{
            abort(404, 'Page is not existed');
        }

    }


    public function save(MenuAdditionRequest $request)
    {
        $quote = $request->input('quote');
        $team = $request->input('team');
        if ($request->has('menu_addition_ids')) {
            $additions = $request->input('menu_addition_ids');
        } else {
            $additions = [];
        }
        $quote->menuAdditions()->sync($additions);

        //Sending and email with pdf file
        MailService::mailFinishedQuote($quote);

        return parent::saveAndContinue($request, $quote, $team);
    }
}
