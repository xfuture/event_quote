<?php

namespace EventQuote\Http\Controllers\Quote\Pages;

use EventQuote\Facades\Room;
use EventQuote\Http\Requests\Quote\RoomRequest;
use Illuminate\Http\Request;
use EventQuote\Http\Controllers\Controller;
use EventQuote\Http\Controllers\Quote\QuotePageController;
use EventQuote\Services\RoomService;

class RoomController extends QuotePageController
{
    public static $template = 'quote.pages.room';
    public static $viewRoute = 'quote.room';
    public static $saveRoute = 'quote.room.save';
    public static $displayName = 'Event Location';
    public static $summary = '_partials.quote.summary';
    public static $id = 2;


    public function view(Request $request)
    {
        $team = $request->input("team");

        if(!empty($team) && count($team->rooms)){
            $rooms = Room::getPublic();

            return parent::view($request)
                ->with('rooms', $rooms);
        }else{
            abort(404, 'Page is not existed');
        }
    }


    public function save(RoomRequest $request)
    {
        $quote = $request->input('quote');
        $team = $request->input('team');
        $quote->room_id = $request->input('room_id');

        return parent::saveAndContinue($request, $quote, $team);
    }
}
