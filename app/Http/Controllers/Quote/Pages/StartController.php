<?php

namespace EventQuote\Http\Controllers\Quote\Pages;

use EventQuote\Http\Requests\Quote\QuoteRequest;
use EventQuote\Services\QuotePagesService;
use EventQuote\Services\QuoteService;
use EventQuote\Services\UserService;
use Illuminate\Http\Request;
use EventQuote\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use EventQuote\User;
use EventQuote\Quote;
use EventQuote\Http\Controllers\Quote\QuotePageController;
use Illuminate\Support\Facades\Log;

class StartController extends QuotePageController
{
    public static $template = 'quote.pages.start';
    public static $viewRoute = 'quote.start';
    public static $saveRoute = 'quote.save';
    public static $displayName = 'Customer & Event Details';
    public static $summary = '_partials.quote.summary';
    public static $id = 1;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var QuoteService
     */
    protected $quoteService;


    public function __construct(QuotePagesService $pagesService, UserService $userService, QuoteService $quoteService)
    {
        parent::__construct($pagesService);
        $this->userService = $userService;
        $this->quoteService = $quoteService;
    }


    public function save(QuoteRequest $request)
    {
        $customer = $this->userService->newOrMatching($request->email, $request->name, preg_replace('/\s+/', '', $request->phone));

        // Save quote
        $quote = $request->input('quote');
        $team = $request->input('team');
        if (is_null($quote)) {
            $quote = new Quote([
                'quote_uid' => uniqid(),
                'customer_id' => $customer->id,
                'function_type_id' => $request->function_type_id,
                'date' => strtotime($request->date),
                'number_people' => $request->number_people,
                'active' => 1,
            ]);
            $quote->team_id = $team->id;
        } else {
            $quote->fill([
                'customer_id' => $customer->id,
                'function_type_id' => $request->function_type_id,
                'date' => strtotime(trim(str_replace('/', '-', str_replace('-', '', $request->date)))),
                'number_people' => $request->number_people,
                'active' => 1
            ]);
        }

        return parent::saveAndContinue($request, $quote, $team);
    }
}
