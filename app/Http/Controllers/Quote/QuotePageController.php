<?php

namespace EventQuote\Http\Controllers\Quote;

use EventQuote\Team;
use EventQuote\Quote;
use EventQuote\Services\QuotePagesService;
use Illuminate\Http\Request;
use EventQuote\Http\Controllers\Controller;

/**
 * The default controller class for all quote pages.
 * Provides some useful default functionality for other pages.
 * @package EventQuote\Http\Controllers\Quote
 */
class QuotePageController extends Controller
{
    /**
     * @var string The route that will be displayed when viewing this page.
     */
    public static $viewRoute = '';

    /**
     * @var string The route that will be used to save the data on this page.
     */
    public static $saveRoute = '';

    /**
     * @var string Name of the page that will be displayed to the user
     */
    public static $displayName = '';

    /**
     * @var string Name of the template to display when viewing the route.
     */
    public static $template = '';

    /**
     * @var int The unique id of this page. Must be a power of 2.
     */
    public static $id = 0;

    /**
     * @var string Name of summary view for this page
     */
    public static $summary = '';

    /**
     * @var QuotePagesService
     */
    protected $pagesService;



    public function __construct(QuotePagesService $pagesService)
    {
        $this->pagesService = $pagesService;
    }


    public function view(Request $request)
    {
        $quote = $request->input('quote');
        $team = $request->input('team');
        return $this->pagesService->renderPage($this, $quote, $team);
    }


    /**
     * Will calls save on the given quote after setting this page complete.
     * This function should only be called from a child controller if the
     * save was successful as this will update the model and continue to
     * the next page.
     *
     * @param Request $request
     * @param Quote $quote
     * @return mixed
     */
    public function saveAndContinue(Request $request, Quote $quote, Team $team)
    {
        $quote->markPageComplete($this::$id);
        $quote->save();

        return $this->pagesService->cont($this, $request, $quote, $team);
    }
}
