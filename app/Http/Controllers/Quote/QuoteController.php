<?php

namespace EventQuote\Http\Controllers\Quote;

use EventQuote\Http\Requests\Quote\MenuAdditionRequest;
use EventQuote\Http\Requests\Quote\QuoteRequest;
use EventQuote\Http\Requests\Quote\RoomRequest;
use EventQuote\MenuAddition;
use EventQuote\Quote;
use EventQuote\Services\RoomService;
use EventQuote\User;
use EventQuote\Menu;
use Illuminate\Http\Request;
use EventQuote\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use EventQuote\Services\QuotePagesService;

class QuoteController extends Controller
{
    public function review(Request $request)
    {
        $quote = $request->input('quote');
        if (!$quote->submitted) {
            abort(404, 'No completed quote could be found under that name');
        }

        return view('quote.review')
            ->with('quote', $quote)
            ->with('menus', null);
    }
}
