<?php

namespace EventQuote\Http\Requests\Quote;

use Illuminate\Foundation\Http\FormRequest;

class MenuAdditionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'menu_addition_ids' => 'array',
//            'menu_addition_ids.*' => 'numeric|exists:menu_additions,id'
        ];
    }
}
