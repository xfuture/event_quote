<?php

namespace EventQuote\Http\Requests\Quote;

use Illuminate\Foundation\Http\FormRequest;

class QuoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'             => 'required|max:255',
            'email'            => 'required|email',
            'phone'            => 'required',
			'function_type_id' => 'required|exists:function_types,id',
            'date'             => 'required|date_format:d/m/Y|after_or_equal:today',
            'number_people'    => 'required|numeric|min:1'
        ];
    }
}
