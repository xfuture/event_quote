<?php

namespace EventQuote\Http\Middleware\Quote;

use Closure;
use EventQuote\Quote;
use EventQuote\Services\QuotePagesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Checks the stage of the quote to confirm that it is passed a certain level.
 * Must be run after the other quote middleware.
 *
 * @package EventQuote\Http\Middleware\Quote
 */
class CheckStage
{
    protected $pagesService;


    public function __construct(QuotePagesService $pagesService)
    {
        $this->pagesService = $pagesService;
    }


    /**
     * Checks to see if the incoming quote has completed the page before the
     * given one. If it has not it will throw an exception if the request
     * is post or redirect to the previous page if it is a get.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Find the page that we are currently routing towards
        $page = $this->pagesService->findPageByRequest($request);
        if ($page !== null)
        {
            // Check if the quote has completed the page beforehand
            if ($request->has('quote')) {
                $quote = $request->input('quote');
                if (!$quote->hasCompletedPage($page::$id / 2)) {
                    return $this->handleInvalidQuote($request, $quote, $page);
                }
            } else {
                abort(500, 'CheckStage middleware is required to run after the other quote middleware');
            }
        }

        return $next($request);
    }


    protected function handleInvalidQuote(Request $request, Quote $quote, $page)
    {
        if ($request->method() === 'GET') {
            $prevPage = $this->pagesService->getPrevPage($page);
            return redirect()
                ->route($prevPage::$viewRoute, $quote->quote_uid);
        } else {
            abort(403, 'Quote has not completed the previous page.');
        }
    }
}
