<?php

namespace EventQuote\Http\Middleware\Quote;

use Closure;
use EventQuote\Quote;
use EventQuote\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Searches for a quote with the given id in the database. If an id is not supplied or it could not be found
 * in the database, redirects.
 * @package EventQuote\Http\Middleware\Quote
 */
class QuoteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param bool $optional Whether a quote is required for this request.
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $optional = false)
    {
        if($request->route('slug')){
            $team = Team::where('slug', $request->route('slug'))->first();
            if (is_null($team))
            {
                abort(404, 'No team with that slug exists');
            }
            $request->merge(['team' => $team]);
        }

        if ($request->route('quoteUid'))
        {
            $quote = Quote::where('quote_uid', $request->route('quoteUid'))->first();
            if (is_null($quote))
            {
                abort(404, 'No quote with that id exists');
            }
            $request->merge(['quote' => $quote]);
        }
        else if (!$optional)
        {
            abort(500, 'A quote is required for this route but no route parameter named quoteUid was present');
        }


        return $next($request);
    }
}
