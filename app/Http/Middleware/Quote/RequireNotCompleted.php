<?php

namespace EventQuote\Http\Middleware\Quote;

use Closure;

class RequireNotCompleted
{
    /**
     * Ensures that the quote has not been completed.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param bool $optional True if a completed quote is required for this
     * route. If false, it won't throw an exception if there is no quote
     * attached to the request, but it will still redirect if the quote is not
     * complete.
     * @return mixed
     */
    public function handle($request, Closure $next, $optional = false)
    {
        // Check if the quote has completed the page beforehand
        if ($request->has('quote')) {
            $quote = $request->input('quote');
            if ($quote->submitted) {
                return redirect()
                    ->route('quote.review', $quote->quote_uid);
            }
        } elseif (!$optional) {
            abort(500, 'RequireNotCompleted middleware is required to run after the other quote middleware');
        }

        return $next($request);
    }
}
