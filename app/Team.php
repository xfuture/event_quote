<?php

namespace EventQuote;

use Laravel\Spark\Team as SparkTeam;

class Team extends SparkTeam
{
    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    public function menus()
    {
        return $this->hasMany(Menu::class);
    }

    public function quotes()
    {
        return $this->hasMany(Quote::class);
    }

    public function menuAdditions()
    {
        return $this->hasMany(MenuAddition::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id', 'id');
    }
}
