<?php

namespace EventQuote;

use EventQuote\Traits\Activateable;
use EventQuote\Traits\BelongsToTeam;
use EventQuote\Traits\Orderable;
use EventQuote\Traits\TracksOwnerAndUpdater;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Menu extends Model
{
    use BelongsToTeam, Activateable, Orderable, TracksOwnerAndUpdater;

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'name',
        'price_pp',
        'features',
        'abstract',
        'description',
        'min_persons',
        'max_persons',
        'order',
        'active'
    ];


    public function menuAdditions()
    {
        return $this->belongsToMany('EventQuote\MenuAddition');
    }


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->ownerIDField = 'owner_id';
        $this->updaterIDField = 'user_id';
    }
}
