<?php

namespace EventQuote;

use EventQuote\Traits\Activateable;
use EventQuote\Traits\BelongsToTeam;
use EventQuote\Traits\Orderable;
use EventQuote\Traits\TracksOwnerAndUpdater;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Builder;

class Room extends Model
{
    use BelongsToTeam, Activateable, Orderable, TracksOwnerAndUpdater;

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'name',
        'caption',
        'abstract',
        'description',
        'order',
        'active'
    ];


    public function quotes()
    {
        return $this->hasMany(Quote::class);
    }


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->ownerIDField = 'owner_id';
        $this->updaterIDField = 'user_id';
    }
}
