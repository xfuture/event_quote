<?php

namespace EventQuote;

use Illuminate\Database\Eloquent\Model;

class FunctionType extends Model
{
    public function quotes()
    {
        return $this->hasMany('EventQuote\Quote');
    }
}
