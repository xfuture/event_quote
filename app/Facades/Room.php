<?php

namespace EventQuote\Facades;

use EventQuote\Services\TeamService;
use Illuminate\Support\Facades\Facade;

class Room extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'room';
    }
}
