<?php

namespace EventQuote\Facades;

use EventQuote\Services\TeamService;
use Illuminate\Support\Facades\Facade;

class Quote extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'quote';
    }
}
