<?php

namespace EventQuote\Traits;

use EventQuote\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

trait TracksOwnerAndUpdater
{
    /**
     * @var string The name of the field that will be updated when the resource
     * is created.
     */
    protected $ownerIDField = 'owner_id';

    /**
     * @var string The name of the field that will be updated when the resource
     * is updated.
     */
    protected $updaterIDField = 'user_id';


    public function owner()
    {
        return $this->belongsTo(User::class, $this->ownerIDField);
    }

    public function updater()
    {
        return $this->belongsTo(User::class, $this->updaterIDField);
    }


    /**
     * Updates the owner of the model.
     *
     * @param User $newOwner
     */
    public function setOwner(User $newOwner)
    {
        if ($this->hasOwnerField()) {
            $this->{$this->ownerIDField} = $newOwner->id;
        }
    }

    /**
     * Updates the 'updater' of the model.
     *
     * @param User $newUpdater
     */
    public function setUpdater(User $newUpdater)
    {
        if ($this->hasUpdaterField()) {
            $this->{$this->updaterIDField} = $newUpdater->id;
        }
    }

    /**
     * Returns true if this model has an owner field.
     *
     * @return bool
     */
    public function hasOwnerField()
    {
        return !is_null($this->ownerIDField);
    }

    /**
     * Returns true if this model has an updater field.
     *
     * @return bool
     */
    public function hasUpdaterField()
    {
        return !is_null($this->updaterIDField);
    }

    /**
     * Scope the query to only include models udpated by a certain user.
     *
     * @param Builder $query
     * @param $id
     * @return Builder
     */
    public function scopeUpdatedBy(Builder $query, $id)
    {
        return $query->where($this->updaterIDField, $id);
    }

    /**
     * Scope the query to only include models created by a certain user.
     *
     * @param Builder $query
     * @param $id
     * @return Builder
     */
    public function scopeCreatedBy(Builder $query, $id)
    {
        return $query->where($this->ownerIDField, $id);
    }
}