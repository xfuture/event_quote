<?php

namespace EventQuote\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Orderable
{
    /**
     * Boot the orderable trait for a model.
     */
    public static function bootOrderable()
    {
        static::addGlobalScope('order', function(Builder $builder) {
            $builder->orderBy('order');
        });
    }

    /**
     * Returns the next available order index for this model.
     *
     * @return int
     */
    public function nextOrderIndex()
    {
        return static::max('order');
    }
}