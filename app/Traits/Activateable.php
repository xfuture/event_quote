<?php

namespace EventQuote\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Activateable
{
    /**
     * Filters the query to only select active results.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query)
    {
        return $query->where('active', 1);
    }
}