<?php

namespace EventQuote\Traits;

use EventQuote\Facades\Team;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

trait BelongsToTeam
{
    protected static $teamIDFieldName = 'team_id';


    public function team()
    {
        return $this->belongsTo(Team::class, static::$teamIDFieldName);
    }


    /**
     * Boot the activateable trait for a model.
     */
    public static function bootBelongsToTeam()
    {
        if(!empty(Request::input('team'))){
            static::addGlobalScope('currentTeam', function(Builder $builder) {
                $builder->where(static::$teamIDFieldName, Request::input('team')->id);
            });
        }elseif(!empty(Auth::user())){
            static::addGlobalScope('currentTeam', function(Builder $builder) {
                $builder->where(static::$teamIDFieldName,Auth::user()->currentTeam()->id);
            });
        }
    }

    /**
     * Selects all models that belong to a particular team.
     *
     * @param int $teamID
     * @param Builder $query
     * @return Builder
     */
    public function scopeForTeam(Builder $query, $teamID)
    {
        return $query
            ->withoutGlobalScope('currentTeam')
            ->where(static::$teamIDFieldName, $teamID);
    }

    /**
     * Selects all models regardless of team.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeForAllTeams(Builder $query)
    {
        return $query
            ->withoutGlobalScope('currentTeam');
    }
}