<?php

namespace EventQuote\Services;


use EventQuote\Mail\FinishedQuote;
use EventQuote\Quote;
use Illuminate\Support\Facades\Mail;

class MailService
{
    public static function mailFinishedQuote(Quote $quote)
    {
        $finishedQuote = new FinishedQuote($quote);
        $customer = $quote->user;
        $team = $quote->team;
        $owner = $team ? $team->owner : null;
        //Mail to user
        if(!empty($customer->email))
        {
            Mail::to($customer->email)->queue($finishedQuote);
        }

        //Mail to team
        if(!empty($owner) && !empty($owner->email))
        {
            Mail::to($owner->email)->queue($finishedQuote);
        }
    }
}