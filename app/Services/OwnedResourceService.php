<?php

namespace EventQuote\Services;

class OwnedResourceService extends TeamResourceService
{
    /**
     * @inheritdoc
     */
    protected function onSave($model)
    {
        $user = $this->userService->current();
        $model->setOwner($user);
        parent::onSave($model);
    }

    /**
     * @inheritdoc
     */
    protected function onCreate($model)
    {
        $user = $this->userService->current();
        $model->setOwner($user);
        $model->setUpdater($user);
        parent::onCreate($model);
    }
}