<?php

namespace EventQuote\Services;

use EventQuote\Menu;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class BaseResourceService
{
    /**
     * @var TeamService
     */
    protected $teamService;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var string Fully qualified name of the resource this service is
     * managing.
     */
    protected $resourceClass;



    public function __construct(TeamService $teamService, UserService $userService)
    {
        $this->teamService = $teamService;
        $this->userService = $userService;
    }


    /**
     * Returns all resources associated with a team.
     *
     * @return Collection
     */
    public function get()
    {
        return ($this->resourceClass)::all();
    }

    /**
     * Returns all resources.
     *
     * @return Collection
     */
    public function getAll()
    {
        return ($this->resourceClass)::forAllTeams()->get();
    }

    /**
     * Returns all resources that should be displayed on the outside site.
     *
     * @return Collection
     */
    public function getPublic()
    {
        return ($this->resourceClass)::active()->get();
    }

    /**
     * Returns true if the resource with the given id belongs to the current
     * team.
     *
     * @param int $id
     * @return bool
     */
    public function belongsToCurrentTeam($id)
    {
        return ($this->resourceClass)::where('id', $id)->exists();
    }

    /**
     * Returns the resource with the matching id.
     *
     * @param int $id
     * @return mixed|null The matching resource or null if no resource with
     * that id exists for the current team.
     */
    public function find($id)
    {
        return ($this->resourceClass)::find($id);
    }

    /**
     * Deletes the resource with the matching id.
     *
     * @param int $id
     * @return boolean True if the resource was deleted, false if there was no
     * matching record in the database.
     */
    public function delete($id)
    {
        $deletedCount = ($this->resourceClass)::destroy($id);
        return $deletedCount > 0;
    }

    /**
     * Updates a menu with a matching id.
     *
     * @param int $id
     * @param array $props New data to assign to the room.
     * @return Menu|null The updated menu or null if there was no menu
     * matching the id in the table.
     */
    public function update($id, $props)
    {
        $resource = $this->find($id);
        if ($resource) {
            $resource->fill($props);

            $this->onSave($resource);

            $resource->save();
            return $resource;
        }
        return null;
    }

    /**
     * Creates a new menu.
     *
     * @param array $props Attributes of the new menu.
     * @return Menu The newly created menu.
     */
    public function create($props)
    {
        $resource = new $this->resourceClass;
        $resource->fill($props);

        $this->onCreate($resource);
        $resource->save();

        return $resource;
    }

    /**
     * Called just before a model is saved to the database. Lets child services
     * change the properties of the model as it's saved.
     *
     * @param Model $model
     */
    protected function onSave($model)
    {
        $user = $this->userService->current();
        $model->setOwner($user);
    }

    /**
     * Called just before a model is created in the database. Lets child
     * services change the properties of the model as it's saved.
     *
     * @param Model $model
     */
    protected function onCreate($model)
    {
        $user = $this->userService->current();
        $team = $this->teamService->current();

        $model->team_id = $team->id;
        $model->setOwner($user);
        $model->setUpdater($user);
    }
}