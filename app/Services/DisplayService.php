<?php

namespace EventQuote\Services;

class DisplayService
{
    public static function displayDate($date)
    {
        return date('jS F Y', $date);
    }

    public static function displayCurrency($number)
    {
        return number_format($number, 2, '.', ',');
    }
}