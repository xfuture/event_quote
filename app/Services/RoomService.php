<?php

namespace EventQuote\Services;

use EventQuote\Room;
use EventQuote\Team;
use Illuminate\Support\Facades\Log;

class RoomService extends BaseResourceService
{
	public static function search(Team $team = null)
	{
		$rooms = new Room();
		
		$rooms = $rooms->get();
		
		return $rooms;
	}

	/**
     * @inheritdoc
     */
	protected $resourceClass = Room::class;
}