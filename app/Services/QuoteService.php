<?php

namespace EventQuote\Services;

use EventQuote\Quote;
use Illuminate\Database\Eloquent\Collection;

class QuoteService extends OwnedResourceService
{
    /**
     * @inheritdoc
     */
    protected $properties = [
        'quote_uid',
        'customer_id',
        'function_type_id',
        'date',
        'number_people',
        'active'
    ];

    /**
     * @inheritdoc
     */
    protected $resourceName = 'quotes';

    /**
     * @inheritdoc
     */
    protected $resourceClass = Quote::class;

    /**
     * @inheritdoc
     */
    protected $ordered = false;


}