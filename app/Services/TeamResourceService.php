<?php

namespace EventQuote\Services;

use EventQuote\Menu;
use EventQuote\Team;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class TeamResourceService
{
    /**
     * @var TeamService
     */
    protected $teamService;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var array Properties of the menu that are written to the model when
     * calling update or create.
     */
    protected $properties = [
    ];

    /**
     * @var string Lowercase name of the resource to access it from the current
     * team.
     */
    protected $resourceName;

    /**
     * @var string Fully qualified name of the resource this service is
     * managing.
     */
    protected $resourceClass;

    /**
     * @var bool True if the resource has the order field.
     */
    protected $ordered = true;


    public function __construct(TeamService $teamService, UserService $userService)
    {
        $this->teamService = $teamService;
        $this->userService = $userService;
    }


    /**
     * Returns all resources associated with a team.
     *
     * @return Collection
     */
    public function get()
    {
        return ($this->resourceClass)::all();
    }

    /**
     * Returns the resource with the matching id.
     *
     * @param int $id
     * @return mixed|null The matching resource or null if no resource with
     * that id exists for the current team.
     */
    public function find($id)
    {
        return ($this->resourceClass)::find($id);
    }

    /**
     * Deletes the resource with the matching id.
     *
     * @param int $id
     * @return boolean True if the resource was deleted, false if there was no
     * matching record in the database.
     */
    public function delete($id)
    {
        $deletedCount = ($this->resourceClass)::destroy($id);
        return $deletedCount > 0;
    }

    /**
     * Updates a menu with a matching id.
     *
     * @param int $id
     * @param array $data New data to assign to the room.
     * @return Menu|null The updated menu or null if there was no menu
     * matching the id in the table.
     */
    public function update($id, $data)
    {
        $resource = ($this->resourceClass)::find($id);
        if ($resource) {
            $props = array_only($data, $this->properties);
            $resource->forceFill($props);
            $this->onSave($resource);
            $resource->save();
            return $resource;
        }
        return null;
    }

    /**
     * Creates a new menu.
     *
     * @param array $data Attributes of the new menu.
     * @return Menu The newly created menu.
     */
    public function create($data)
    {
        $props = array_only($data, $this->properties);
//        $props = $this->editCreateProps($props);

        $resource = new $this->resourceClass;
        $resource->forceFill($props);
        $this->onCreate($resource);
        $resource->save();
        return $resource;
    }

    /**
     * Called just before a model is saved to the database. Lets child services
     * change the properties of the model as it's saved.
     *
     * @param Model $model
     */
    protected function onSave($model)
    {

    }

    /**
     * Called just before a model is created in the database. Lets child
     * services change the properties of the model as it's saved.
     *
     * @param Model $model
     */
    protected function onCreate($model)
    {
        $model->team_id = $this->teamService->current()->id;
    }
//
//    /**
//     * Change the properties that have been passed to the update route before
//     * they are persisted to the database.
//     *
//     * @param array $props Attributes that will be filled onto the model.
//     * @return array The updated properties.
//     */
//    protected function editUpdateProps($props)
//    {
//        return $props;
//    }
//
//    /**
//     * Change the properties that have been passed to the create route before
//     * they are persisted to the database.
//     *
//     * @param array $props Attributes that will be filled onto the model.
//     * @return array The updated properties.
//     */
//    protected function editCreateProps($props)
//    {
//        $props['team_id'] = $this->teamService->current()->id;
//        $props['user_id'] = $this->userService->current()->id;
////        $props['owner_id'] = $this->userService->current()->id;
//
////        $props = array_add($props, 'active', 1);
////        if ($this->ordered && !in_array('order', $props)) {
////            $props['order'] = ($this->resourceClass)::max('order') + 1;
////        }
//
//        return $props;
//    }
}