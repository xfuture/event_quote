<?php

namespace EventQuote\Services;

class TeamService
{
    /**
     * @var UserService
     */
    protected $userService;


    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    /**
     * Get the currently active team from the current user.
     *
     * @return \EventQuote\Team|null Returns the currently active team or null
     * if there is no user logged in or if the user is not part of any teams.
     */
    public function current()
    {
        $user = $this->userService->current();
        if ($user && $user->currentTeam) {
            return $user->currentTeam;
        }

        return null;
    }
}