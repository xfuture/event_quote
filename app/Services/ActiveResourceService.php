<?php

namespace EventQuote\Services;

class ActiveResourceService extends OwnedResourceService
{
    /**
     * Returns all active resources.
     *
     * @return Collection
     */
    public function getActive()
    {
        return ($this->resourceClass)::active()->get();
    }

    /**
     * Returns a single active resource.
     *
     * @param int $id
     * @return mixed|null The matching resource or null if no resource with
     * that id is active.
     */
    public function findActive($id)
    {
        return ($this->resourceClass)::active()->where('id', $id);
    }
}