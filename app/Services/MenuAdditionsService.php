<?php

namespace EventQuote\Services;

use EventQuote\Menu;
use EventQuote\MenuAddition;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class MenuAdditionsService extends BaseResourceService
{
    /**
     * @inheritdoc
     */
    protected $resourceClass = \EventQuote\MenuAddition::class;


    /**
     * Returns all menu additions should be displayed for a given menu.
     *
     * @param Menu $menu
     * @return Collection
     */
    public function getAllMenuAdditions(Menu $menu)
    {
        if (!is_null($menu)) {
            return MenuAddition::forMenu($menu)->get();
        }
        return new Collection();
    }

    /**
     * Returns all menu additions should be displayed for a given menu.
     *
     * @param Menu $menu
     * @return Collection
     */
    public function getAllActiveMenuAdditions(Menu $menu = null)
    {
        if (!is_null($menu)) {
            return MenuAddition::forMenu($menu->id)->active()->get();
        }
        return new Collection();
    }
}