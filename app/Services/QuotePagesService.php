<?php
/**
 * Created by PhpStorm.
 * User: dcodegroup
 * Date: 27/02/17
 * Time: 3:18 PM
 */

namespace EventQuote\Services;

use EventQuote\Http\Controllers\Quote\Pages\FinishController;
use EventQuote\Http\Controllers\Quote\Pages\MenuAdditionsController;
use EventQuote\Http\Controllers\Quote\Pages\MenuController;
use EventQuote\Http\Controllers\Quote\Pages\RoomController;
use EventQuote\Http\Controllers\Quote\Pages\StartController;
use EventQuote\Quote;
use EventQuote\Http\Controllers\Quote\QuotePageController;
use EventQuote\Team;
use Illuminate\Http\Request;


class QuotePagesService
{
    /**
     * @var array An array of FQ class names of page controllers.
     */
    private $pageClasses = [
        1=>StartController::class,
        2=>RoomController::class,
        4=>MenuController::class,
        8=>MenuAdditionsController::class,
        16=>FinishController::class
    ];


    /**
     * Searches through the pages and returns the first one where the
     * value of the static property matches the value given.
     */
    public function findPage($prop, $value)
    {
        $temp = null;
        while (list($key, $class) = each($this->pageClasses)) {
            if ($class::$$prop == $value) {
                $temp = $class;
            }
        }
        reset($this->pageClasses);
        return $temp;
    }


    /**
     * Checks which page the given request is directed towards.
     */
    public function findPageByRequest(Request $request)
    {
        $routeName = $request->route()->getName();
        $page = $this->findPage('viewRoute', $routeName);
        if (!is_null($page)) {
            return $page;
        }

        $page = $this->findPage('saveRoute', $routeName);
        if (!is_null($page)) {
            return $page;
        }

        return null;
    }


    /**
     * Returns the next page after the given one or null if there isn't one.
     * @param string $curPage Class name of the current path.
     * @return string|null
     */
    public function getNextPage($curPage, Team $team =null, Quote $quote = null)
    {
        while (list($key, $value) = each($this->pageClasses)) {
            if ($value == $curPage) {
                if($team !== null && $quote != null){
                    //Check for the next page if there is no menu
                    if((!count($team->rooms) && $curPage == StartController::class) ||
                        (!count($team->menuAdditions) && $curPage == MenuController::class)){
                        next($this->pageClasses);
                        $quote->markPageComplete($key * 2);
                        $quote->save();
                    }
                }
                break;
            }
        }

        $page = current($this->pageClasses);
        reset($this->pageClasses);
        return ($page === false ? null : $page);
    }


    /**
     * Returns the previous page before the given one or null if there isn't one.
     * @param string $curPage Class name of the current path.
     * @return string|null
     */
    public function getPrevPage($curPage)
    {
        $array = array_reverse($this->pageClasses);
        while (list($key, $value) = each($array)) {
            if ($value == $curPage) {
                break;
            }
        }

        $page = current($array);
        return ($page === false ? null : $page);
    }


    /**
     * Returns the page data for this quote.
     *
     * @param QuotePageController $currentController Current page.
     * @param \EventQuote\Quote $quote Quote object.
     * @return array Details of each other page in the quote.
     */
    protected function getPagesFor($currentController, Quote $quote = null)
    {
        $result = [];
        $accessible = true;
        foreach ($this->pageClasses as $pageCtrl) {
            $result[$pageCtrl::$displayName] = [
                'isCurrent' => $pageCtrl::$id == $currentController::$id,
                'canAccess' => $accessible,
                'route' => $pageCtrl::$viewRoute,
                'displayName' => $pageCtrl::$displayName,
            ];

            // Ensures that only completed pages and the next page to complete are available.
            if (is_null($quote) || !$quote->hasCompletedPage($pageCtrl::$id)) {
                $accessible = false;
            }
        }

        return $result;
    }


    /**
     * Renders a template with a few additional variables.
     *
     * @param QuotePageController $pageController Page to render.
     * @param bool $withLayout Whether to wrap the page in a layout.
     * @param \EventQuote\Quote $quote Quote object.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View View object of the page.
     */
    protected function renderWithVars($pageController, $withLayout, Quote $quote = null, Team $team = null)
    {
        if ($withLayout) {
            $view = view('quote.pageWrapper');
        } else {
            $view = view($pageController::$template);
        }
        return $view
            ->with('quotePages', $this->getPagesFor($pageController, $quote))
            ->with('currentQuotePage', $pageController)
            ->with('hasPrevQuotePage', !is_null($this->getPrevPage(get_class($pageController))))
            ->with('hasNextQuotePage', !is_null($this->getNextPage(get_class($pageController))))
            ->with('pageHasData', true)
            ->with('quote', $quote)
            ->with('team', $team);
    }


    /**
     * Renders the full page of a quote.
     *
     * @param QuotePageController $pageController Page to render.
     * @param \EventQuote\Quote $quote Quote object.
     * @return \Illuminate\View\View View object of the page.
     */
    public function renderPage($pageController, Quote $quote = null, Team $team = null)
    {
        return $this->renderWithVars($pageController, true, $quote, $team);
    }


    /**
     * Renders just the page of the quote without any layout.
     *
     * @param QuotePageController $pageController Page to render.
     * @param \EventQuote\Quote $quote Quote object.
     * @return \Illuminate\View\View View object of the page.
     */
    public function renderRawPage($pageController, Quote $quote = null, Team $team = null)
    {
        return $this->renderWithVars($pageController, false, $quote, $team);
    }


    /**
     * Continues the booking flow based on the value of the submit input.
     *
     * @param QuotePageController $pageController Page to render.
     * @param \Illuminate\Http\Request $request The request object sent to the current route.
     * @param \EventQuote\Quote $quote Quote object.
     * @return \Illuminate\View\View A redirect to the next page of the request.
     */
    public function cont($pageController, $request, Quote $quote, Team $team)
    {
        $redirect = $request->input('redirect');
        if ($redirect == 'next') {
            // Redirecting to the next page of the quote

            $next = $this->getNextPage(get_class($pageController), $team, $quote);
            if (!is_null($next)) {

                return redirect()
                    ->route($next::$viewRoute, [$team->slug, $quote->quote_uid]);
            } else {
                abort(404, 'There is no next page');
                // Return to the menu if this was the final screen
//                return redirect()
//                    ->route('quote.review', [$quote->quote_uid])
//                    ->with('flashNotice', 'Room booked.');
            }

        } elseif ($redirect == 'prev') {
            // Redirect to the previous page of the quote
            $prev = $this->getPrevPage(get_class($pageController));
            if (!is_null($prev)) {
                return redirect()
                    ->route($prev::$viewRoute, [$team->slug, $quote->quote_uid]);
            }
        } else {
            // Redirect to the selected page
            return redirect()
                ->route($redirect, [$team->slug,  $quote->quote_uid]);
        }
    }
}





















