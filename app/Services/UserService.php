<?php

namespace EventQuote\Services;

use EventQuote\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserService
{
    /**
     * @var \EventQuote\User Override the current user. By default the current
     * user is fetched from the Auth facade. If this property is set to a user
     * instance then that will be used instead.
     */
    protected $currentUser = null;


    public function __construct()
    {

    }

    /**
     * Fetches the currently authenticated user or null if there isn't one.
     *
     * @return \EventQuote\User|null The current user.
     */
    public function current()
    {
        if (!is_null($this->currentUser)) {
            return $this->currentUser;
        }
        return Auth::user();
    }

    /**
     * Find an existing matching user or create a new one.
     *
     * This function will search for a user with a matching email address. If
     * one exists, then it will update the name and phone number of the record
     * and save it. If there is no matching user one will be created with a
     * default password.
     *
     * @param $email
     * @param $name
     * @param $phone
     * @return User New or matching user object.
     */
    public function newOrMatching($email, $name, $phone)
    {
        $customer = User::withEmail($email)->first();
        if (is_null($customer)) {
            $customer = new User(['email' => $email]);
            $customer->password = Hash::make(uniqid());
        }
        $customer->name = $name;
        $customer->phone = $phone;
        $customer->save();

        return $customer;
    }
}