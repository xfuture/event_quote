<?php

namespace EventQuote\Services;

use EventQuote\Menu;

class MenuService extends BaseResourceService
{
    /**
     * @inheritdoc
     */
    protected $resourceClass = Menu::class;
}