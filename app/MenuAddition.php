<?php

namespace EventQuote;

use EventQuote\Traits\Activateable;
use EventQuote\Traits\BelongsToTeam;
use EventQuote\Traits\Orderable;
use EventQuote\Traits\TracksOwnerAndUpdater;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuAddition extends Model
{
    use SoftDeletes;
    use BelongsToTeam, Activateable, Orderable, TracksOwnerAndUpdater;

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'name',
        'price_pp',
        'description',
//        'min_persons',
//        'max_persons',
        'order',
        'active'
    ];


    public function menus()
    {
        return $this->belongsToMany('EventQuote\Menu');
    }

    /**
     * Scopes a query to return all menu additions for a certain menu.
     * This is includes globally applied menu items, for only getting menu
     * items that are related to the menu, follow the Eloquent relationship.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeForMenu(Builder $query, $menuID)
    {
        return $query
            ->doesntHave('menus')
            ->orWhereHas('menus', function($q) use($menuID) {
                $q->where('id', $menuID);
            });
    }
}
